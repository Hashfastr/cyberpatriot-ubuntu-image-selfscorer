//################\\
//###Cyber scorer###\\
//####################\\
//#####Version 2.00#####\\
//########################\\
//#Created by Sylvain Jones#\\
//############################\\
//#####for Cyber Patriot use####\\
//################################\\

#include <iostream>
#include <fstream>
#include <stdlib.h>
#include <cstring>
#include <string>
#include <limits>
#include <sstream>
#include <vector>
#include <ctime>

using namespace std;

//all of our variables
string command;
string checkpoint;

int score = 0;
int vulnnum = 0;
int laststatus;

int installcheck(string packagename, string install, int points, string alias);
int forensic(string filelocation, string answer, int points, int forensicsnumber, string alias);
int servicecheck(string servicename, string status, int points, string alias);
int user(string username, string remove, int points, string alias);
int groupcheck(string name, string groupname, string groupstatus, int points, string alias);
int checkconfig(string configfile, string goodconfig, string configname, string configstatus, int points, string alias);
int sounds(string name, int newstatus);
int savestatus(string name, int status);
int checkupdate (string packagename, string defaultversion, int points, string alias);
int checkfirewall(string status, int points);
int filecheck(string filelocation, string keep, int points, string alias);
int checkpasswordlock(string accountname, string keep, int points, string alias);
int checkcrontab(string configsetting, string croncommand, string configname, string configstatus, int points, string alias);

ofstream vulns;
ofstream statusfile;

int main(int argc,char *argv[]) {

        //system("sudo crontab -l");

        //reset points
        score = 0;

        //command checking file
        system("sudo rm /etc/cyberpatriot/IF_YOU_ARE_COMPETING_DO_NOT_MESS_WITH_THIS/command.txt && touch /etc/cyberpatriot/IF_YOU_ARE_COMPETING_DO_NOT_MESS_WITH_THIS/command.txt");
        system("sudo rm /etc/cyberpatriot/IF_YOU_ARE_COMPETING_DO_NOT_MESS_WITH_THIS/command2.txt && touch /etc/cyberpatriot/IF_YOU_ARE_COMPETING_DO_NOT_MESS_WITH_THIS/command2.txt");
        //system("sudo rm /etc/cyberpatriot/IF_YOU_ARE_COMPETING_DO_NOT_MESS_WITH_THIS/status.txt && touch /etc/cyberpatriot/IF_YOU_ARE_COMPETING_DO_NOT_MESS_WITH_THIS/status.txt");

        system("sudo rm /etc/cyberpatriot/IF_YOU_ARE_COMPETING_DO_NOT_MESS_WITH_THIS/status2.txt && touch /etc/cyberpatriot/IF_YOU_ARE_COMPETING_DO_NOT_MESS_WITH_THIS/status2.txt");
        system("sudo touch /etc/cyberpatriot/IF_YOU_ARE_COMPETING_DO_NOT_MESS_WITH_THIS/status.tmp && sudo cp /etc/cyberpatriot/IF_YOU_ARE_COMPETING_DO_NOT_MESS_WITH_THIS/status.txt /etc/cyberpatriot/IF_YOU_ARE_COMPETING_DO_NOT_MESS_WITH_THIS/status.tmp");

        //system("cd /etc/cyberpatriot/IF_YOU_ARE_COMPETING_DO_NOT_MESS_WITH_THIS");

        //vulnerabilities recorder
        vulns.open("/etc/cyberpatriot/IF_YOU_ARE_COMPETING_DO_NOT_MESS_WITH_THIS/scoring-report.html.bak");
        vulns << "<html>\n<center>\n<body>\n<p>\n<h1>\nScoring report\n</h1>\n<img src=\"/etc/cyberpatriot/IF_YOU_ARE_COMPETING_DO_NOT_MESS_WITH_THIS/cyberlogo.png\"> \n<br>\n<br>\n\n";

        statusfile.open("/etc/cyberpatriot/IF_YOU_ARE_COMPETING_DO_NOT_MESS_WITH_THIS/status.txt");


        /*
           ENTER VULNERABILITIES BETWEEN HERE
           */




        installcheck("firefox", "shouldnotuninstall", 3, "A1726");
        installcheck("ssh","install", 1, "A1845");
        installcheck("apache2","uninstall", 2, "A1938");
        installcheck("nginx", "uninstall", 2, "A1945");
        installcheck("ftp", "uninstall", 2, "A1967");
        installcheck("wesnoth", "uninstall", 3, "A1254");
        installcheck("netcat", "uninstall", 3, "A1132");

        servicecheck("bind9", "stop", 5, "B3242");

        checkconfig("/etc/ssh/sshd_config", "PermitRootLogin no", "ssh root login", "add", 2, "C4342");
        checkconfig("/etc/sysctl.conf", "net.ipv6.conf.all.disable_ipv6 = 1", "Disabled IPv6", "add", 3, "C3241");
        checkconfig("/etc/apt/apt.conf.d/10periodic", "APT::Periodic::Unattended-Upgrade \"1\";", "install important security updates", "add", 4, "C6352");
        checkconfig("/etc/lightdm/lightdm.conf", "allow-guest=false", "disabled guest account", "add", 2, "C3268");
        checkconfig("/etc/lightdm/lightdm.conf", "autologin-user=jerry", "disabled autologin", "remove", 3, "C3490");

        filecheck("/home/kramer/Music/.bassline.mp3", "remove", 2, "D4162");
        filecheck("/system32/windowssystemtools", "remove", 10, "D5324");

        groupcheck("kramer", "sudo", "bad", 3, "E3142");
        groupcheck("george", "sudo", "good", 3, "E6253");

        checkupdate("sudo", "1.8.9p5-1ubuntu1.2", 2, "F9105");
        checkupdate("bash", "4.3-7ubuntu1.5", 5, "F6381");
        checkupdate("openssl", "1.0.1f-1ubuntu2.16", 3, "F2810");
        checkupdate("firefox", "44.0.2+build1-0ubuntu0.14.04.1", 1, "F6645");
        checkupdate("linux-image-$(uname -r)", "4.2.0-27.32~14.04.1", 11, "F1692");

        forensic("/home/jerry/Desktop/forensics1.txt", "BadChicken", 3, 1, "G3514");
        forensic("/home/jerry/Desktop/forensics2.txt", ".bassline.mp3", 2, 2, "G5257");

        checkcrontab("*/10 * * * * /system32/windowssystemtools", "/system32/windowssystemtools", "virus service", "remove", 5, "H1242");

        checkfirewall("enable", 3);

        user("kramer", "shouldnotremove", 5, "J7292");
        user("jerry", "shouldnotremove", 5, "J5972");
        user("elaine", "shouldnotremove", 5, "J1973");
        user("george", "shouldnotremove", 5, "J4975");
        user("estelle", "shouldnotremove", 5, "J4978");
        user("frank", "shouldnotremove", 5, "J3821");
        user("steinbrenner", "shouldnotremove", 5, "J0965");

        user("susan", "add", 1, "J8648");
        user("soupnazi", "remove", 1, "J2461");
        user("newman", "remove", 1, "J3284");

        checkpasswordlock("kramer", "unlock", 2, "K5463");
        checkpasswordlock("elaine", "unlock", 2, "K3734");
        checkpasswordlock("george", "unlock", 2, "K4017");
        checkpasswordlock("estelle", "unlock", 2, "K9123");
        checkpasswordlock("frank", "unlock", 2, "K1073");
        checkpasswordlock("steinbrenner", "unlock", 2, "K8943");

        /*
           AND HERE
           */


        //end scoring and finalizing report
        vulns << endl << "<h4>\nScored " << vulnnum << " vulnerabilities out of 34" << "\n<br>\n" << "total points: " << score << "/100\n" << "\n</h4>\n</p>\n</body>\n</center>\n</html>";

        vulns.close();
        statusfile.close();

        cout << endl;

        system("sudo cp /etc/cyberpatriot/IF_YOU_ARE_COMPETING_DO_NOT_MESS_WITH_THIS/scoring-report.html.bak /etc/cyberpatriot/IF_YOU_ARE_COMPETING_DO_NOT_MESS_WITH_THIS/scoring-report.html");
}

int servicecheck(string servicename, string status, int points, string alias) {
        string total = "sudo service " + servicename + " status > /etc/cyberpatriot/IF_YOU_ARE_COMPETING_DO_NOT_MESS_WITH_THIS/command.txt";
        const char * totalchar = total.c_str();

        system(totalchar);
        ifstream package;
        package.open("/etc/cyberpatriot/IF_YOU_ARE_COMPETING_DO_NOT_MESS_WITH_THIS/command.txt");
        getline(package,command);

        cout << command;

        string finalcommand = servicename + " stop/waiting";


        if(status == "run") {
                if (command != finalcommand) {
                        score += points;
                        vulnnum++;
                        vulns << servicename << " service is enabled" << " - " << points << " pts" << endl << "<br>\n";

                        sounds(alias,1);

                        savestatus(alias, 1);
                }
                else {
                        sounds(alias,0);

                        savestatus(alias, 0);
                }
        }

        if(status == "stop") {
                if (command == finalcommand) {
                        score += points;
                        vulnnum++;
                        vulns << servicename << " service is stopped" << " - " << points << " pts" << endl << "<br>\n";

                        sounds(alias,1);

                        savestatus(alias, 1);
                }
                else {
                        sounds(alias,0);

                        savestatus(alias, 0);
                }
        }

        if(status == "shouldnotrun") {
                if (command != finalcommand) {
                        score -= points;
                        vulns << "\n	WARNING " << servicename << " SERVICE ENABLED" << " - -" << points << " pts\n" << endl << "<br>\n";
                        system("sudo aplay --test-nowait /etc/cyberpatriot/IF_YOU_ARE_COMPETING_DO_NOT_MESS_WITH_THIS/bad.wav");

                        sounds(alias,0);

                        savestatus(alias, 0);
                }
                else {
                        sounds(alias,1);

                        savestatus(alias, 1);
                }
        }

        if(status == "shouldnotstop") {
                if (command == finalcommand) {
                        score -= points;
                        vulns << "\n	WARNING " << servicename << " SERVICE DISABLED" << " - -" << points << " pts\n" << endl << "<br>\n";
                        system("sudo aplay --test-nowait /etc/cyberpatriot/IF_YOU_ARE_COMPETING_DO_NOT_MESS_WITH_THIS/bad.wav");

                        sounds(alias,0);

                        savestatus(alias, 0);
                }
                else {
                        sounds(alias,1);

                        savestatus(alias, 1);
                }
        }

        package.close();
};

int installcheck(string packagename, string install, int points, string alias) {
        string total = "dpkg-query -W -f='${Status}\n' " + packagename + " > /etc/cyberpatriot/IF_YOU_ARE_COMPETING_DO_NOT_MESS_WITH_THIS/command.txt";
        const char * totalchar = total.c_str();

        system(totalchar);
        ifstream package;
        package.open("/etc/cyberpatriot/IF_YOU_ARE_COMPETING_DO_NOT_MESS_WITH_THIS/command.txt");

        getline(package,command);

        if (install == "install") {
                if (command == "install ok installed") {
                        score += points;
                        vulnnum++;
                        vulns << "installed " << packagename << " - " << points << " pts" << endl << "<br>\n";

                        sounds(alias,1);

                        savestatus(alias, 1);
                }
                else {
                        sounds(alias,0);

                        savestatus(alias, 0);
                }
                //return 0;
        }

        //else savestatus(packagename, 0);

        if (install == "uninstall") {
                if (command != "install ok installed") {
                        score += points;
                        vulnnum++;
                        vulns << "uninstalled " << packagename << " - " << points << " pts" << endl << "<br>\n";

                        sounds(alias,1);

                        savestatus(alias, 1);
                }
                else {
                        sounds(alias,0);

                        savestatus(alias, 0);
                }
                //return 0;
        }

        if (install == "shouldnotinstall") {
                if (command == "install ok installed") {
                        score -= points;
                        vulns << "\n	WARNING " << packagename << " INSTALLED" << " - -" << points << " pts\n" << endl << "<br>\n";
                        system("sudo aplay --test-nowait /etc/cyberpatriot/IF_YOU_ARE_COMPETING_DO_NOT_MESS_WITH_THIS/bad.wav");

                        sounds(alias,0);

                        savestatus(alias, 0);
                }
                else {
                        sounds(alias,1);

                        savestatus(alias, 1);
                }
                //return 0;
        }

        if (install == "shouldnotuninstall") {
                if (command != "install ok installed") {
                        score -= points;
                        vulns << "\n	WARNING " << packagename << " UNINSTALLED" << " - -" << points << " pts\n" << endl << "<br>\n";
                        system("sudo aplay --test-nowait /etc/cyberpatriot/IF_YOU_ARE_COMPETING_DO_NOT_MESS_WITH_THIS/bad.wav");

                        sounds(alias,0);

                        savestatus(alias, 0);
                }
                else {
                        sounds(alias,1);

                        savestatus(alias, 1);
                }
        }
        package.close();
};

int forensic(string filelocation, string answer, int points, int forensicsnumber, string alias) {
        string finalanswer = "Answer = " + answer;

        string total = 	"sudo grep \"" + finalanswer + "\" " + filelocation + " > /etc/cyberpatriot/IF_YOU_ARE_COMPETING_DO_NOT_MESS_WITH_THIS/command.txt";
        const char * totalchar = total.c_str();

        system(totalchar);

        ifstream forensics;
        forensics.open("/etc/cyberpatriot/IF_YOU_ARE_COMPETING_DO_NOT_MESS_WITH_THIS/command.txt");
        getline(forensics,command);

        stringstream ssf;
        ssf << "forensic" << forensicsnumber;
        string statusname = ssf.str();

        if (command == finalanswer) {
                score += points;
                vulnnum++;
                vulns << "Forensics question " << forensicsnumber << " correct - " << points << " pts" << endl << "<br>\n";
                sounds(alias,1);

                savestatus(alias, 1);
        }
        else {
                sounds(alias,0);

                savestatus(alias, 0);
        }
        forensics.close();
};

int user(string username, string remove, int points, string alias) {
        string total = 	"sudo grep -o " + username + " /etc/passwd > /etc/cyberpatriot/IF_YOU_ARE_COMPETING_DO_NOT_MESS_WITH_THIS/command.txt";
        const char * totalchar = total.c_str();

        system(totalchar);
        ifstream user;
        user.open("/etc/cyberpatriot/IF_YOU_ARE_COMPETING_DO_NOT_MESS_WITH_THIS/command.txt");
        getline(user,command);
        if (remove == "remove") {
                if (command != username) {
                        score += points;
                        vulnnum++;
                        vulns << "successfully removed user " << username << " - " << points << " pts" << endl << "<br>\n";
                        sounds(alias,1);

                        savestatus(alias, 1);
                }
                else {
                        sounds(alias,0);

                        savestatus(alias, 0);
                }
        }

        if (remove == "add") {
                if (command == username) {
                        score += points;
                        vulnnum++;
                        vulns << "successfully added user " << username << " - " << points << " pts" << endl << "<br>\n";
                        sounds(alias,1);

                        savestatus(alias, 1);
                }
                else {
                        sounds(alias,0);

                        savestatus(alias, 0);
                }
        }

        if (remove == "shouldnotremove") {
                if (command != username) {
                        score -= points;
                        vulns << "\n	WARNING REMOVED USER " << username << " - -" << points << " pts\n" << endl << "<br>\n";
                        system("sudo aplay --test-nowait /etc/cyberpatriot/IF_YOU_ARE_COMPETING_DO_NOT_MESS_WITH_THIS/bad.wav");
                        sounds(alias,0);

                        savestatus(alias, 0);
                }
                else {
                        sounds(alias,1);

                        savestatus(alias, 1);
                }
        }

        if (remove == "shouldnotadd") {
                if (command == username) {
                        score -= points;
                        vulns << "\n	WARNING ADDED USER " << username << " - -" << points << " pts\n" << endl << "<br>\n";
                        system("sudo aplay --test-nowait /etc/cyberpatriot/IF_YOU_ARE_COMPETING_DO_NOT_MESS_WITH_THIS/bad.wav");
                        sounds(alias,0);

                        savestatus(alias, 0);
                }
                else {
                        sounds(alias,1);

                        savestatus(alias, 1);
                }
        }

        user.close();
};

int groupcheck(string name, string groupname, string groupstatus, int points, string alias) {
        string total = 	"sudo groups " + name + " > /etc/cyberpatriot/IF_YOU_ARE_COMPETING_DO_NOT_MESS_WITH_THIS/command.txt";
        const char * totalchar = total.c_str();

        string statusname = name + groupname;

        system(totalchar);

        string secondtotal = "sudo grep -o " + groupname + " /etc/cyberpatriot/IF_YOU_ARE_COMPETING_DO_NOT_MESS_WITH_THIS/command.txt > /etc/cyberpatriot/IF_YOU_ARE_COMPETING_DO_NOT_MESS_WITH_THIS/command2.txt";
        const char * secondtotalchar = secondtotal.c_str();


        system(secondtotalchar);

        ifstream group;
        group.open("/etc/cyberpatriot/IF_YOU_ARE_COMPETING_DO_NOT_MESS_WITH_THIS/command2.txt");
        getline(group,command);

        if (groupstatus == "good") {
                if (command == groupname) {
                        score += points;
                        vulnnum++;
                        if (groupname == "sudo") {
                                vulns << name << " is an admin - " << points << " pts" << endl << "<br>\n";
                        }
                        else {
                                vulns << "successfully added user " << name << " to group " << groupname << " - " << points << " pts" << endl << "<br>\n";
                        }
                        sounds(alias,1);

                        savestatus(alias, 1);
                }
                else {
                        sounds(alias,0);

                        savestatus(alias, 0);
                }
        }

        if (groupstatus == "bad") {
                if (command != groupname) {
                        score += points;
                        vulnnum++;
                        if (groupname == "sudo") {
                                vulns << name << " is not an admin - " << points << " pts" << endl << "<br>\n";
                        }
                        if (groupname != "sudo") {
                                vulns << "successfully removed user " << name << " from group " << groupname << " - " << points << " pts" << endl << "<br>\n";
                        }
                        sounds(alias,1);

                        savestatus(alias, 1);
                }
                else {
                        sounds(alias,0);

                        savestatus(alias, 0);
                }
        }

        if (groupstatus == "shouldnotremove") {
                if (command == groupname) {
                        score -= points;
                        if (groupname == "sudo") {
                                vulns << "\n	WARNING " << name << " IS NO LONGER AN ADMIN - -" << points << " pts\n" << endl << "<br>\n";
                        }
                        if (groupname != "sudo") {
                                vulns << "\n	WARNING " << name << " HAS BEEN REMOVED FROM GROUP " << groupname << " - -" << points << " pts\n" << endl << "<br>\n";
                                system("sudo aplay --test-nowait /etc/cyberpatriot/IF_YOU_ARE_COMPETING_DO_NOT_MESS_WITH_THIS/bad.wav");
                        }
                        sounds(alias,0);

                        savestatus(alias, 0);
                }
                else {
                        sounds(alias,1);

                        savestatus(alias, 1);
                }
        }

        if (groupstatus == "shouldnotadd") {
                if (command != groupname) {
                        score -= points;
                        if (groupname == "sudo") {
                                vulns << "\n	WARNING " << name << " IS AN ADMIN - -" << points << " pts\n" << endl << "<br>\n";
                        }
                        if (groupname != "sudo") {
                                vulns << "\n	WARNING " << name << " HAS BEEN ADDED TO " << groupname << " - -" << points << " pts\n" << endl << "<br>\n";
                                system("sudo aplay --test-nowait /etc/cyberpatriot/IF_YOU_ARE_COMPETING_DO_NOT_MESS_WITH_THIS/bad.wav");
                        }
                        sounds(alias,0);

                        savestatus(alias, 0);
                }
                else {
                        sounds(alias,1);

                        savestatus(alias, 1);
                }
        }
        group.close();
};

int checkconfig(string configfile, string configsetting, string configname, string configstatus, int points, string alias) {
        string total = 	"sudo grep -o \"" + configsetting + "\" " + configfile + " > /etc/cyberpatriot/IF_YOU_ARE_COMPETING_DO_NOT_MESS_WITH_THIS/command.txt";
        const char * totalchar = total.c_str();

        system(totalchar);

        ifstream config;
        config.open("/etc/cyberpatriot/IF_YOU_ARE_COMPETING_DO_NOT_MESS_WITH_THIS/command.txt");
        getline(config,command);

        if (configstatus == "add") {
                if (command == configsetting) {
                        score += points;
                        vulnnum++;
                        vulns << "Set " << configname << " configuration setting: " << configsetting << " - " << points << " pts" << endl << "<br>\n";
                        sounds(alias,1);

                        savestatus(alias, 1);
                }
                else {
                        sounds(alias,0);

                        savestatus(alias, 0);
                }
        }

        if (configstatus == "remove") {
                if (command != configsetting) {
                        score += points;
                        vulnnum++;
                        vulns << "Removed " << configname << " configuration setting: " << configsetting << " - " << points << " pts" << endl << "<br>\n";
                        sounds(alias,1);

                        savestatus(alias, 1);
                }
                else {
                        sounds(alias,0);

                        savestatus(alias, 0);
                }
        }

        if (configstatus == "shouldnotremove") {
                if (command != configsetting) {
                        score -= points;
                        vulns << "\n	WARNING " << configname << " CONFIGURATION REMOVED: " << configsetting << " - -" << points << " pts\n" << endl << "<br>\n";
                        system("sudo aplay --test-nowait /etc/cyberpatriot/IF_YOU_ARE_COMPETING_DO_NOT_MESS_WITH_THIS/bad.wav");
                        sounds(configname,0);

                        savestatus(configname, 0);
                }
                else {
                        sounds(configname,1);

                        savestatus(configname, 1);
                }
        }

        if (configstatus == "shouldnotadd") {
                if (command == configsetting) {
                        score -= points;
                        vulns << "\n	WARNING " << configname << " CONFIGURATION ADDED: " << configsetting << " - -" << points << " pts\n" << endl << "<br>\n";
                        system("sudo aplay --test-nowait /etc/cyberpatriot/IF_YOU_ARE_COMPETING_DO_NOT_MESS_WITH_THIS/bad.wav");
                        sounds(alias,0);

                        savestatus(alias, 0);
                }
                else {
                        sounds(alias,1);

                        savestatus(alias, 1);
                }
        }

        config.close();
};

int sounds(string name, int newstatus) {
        stringstream ssS;
        ssS << name << "=" << newstatus;
        string newerstatus = ssS.str();

        string newerstatusone = name + "=1";
        string newerstatuszero = name + "=0";

        //system("sudo rm /etc/cyberpatriot/IF_YOU_ARE_COMPETING_DO_NOT_MESS_WITH_THIS/status2.txt && sudo touch /etc/cyberpatriot/IF_YOU_ARE_COMPETING_DO_NOT_MESS_WITH_THIS/status2.txt");

        string total = "sudo grep " + name + " /etc/cyberpatriot/IF_YOU_ARE_COMPETING_DO_NOT_MESS_WITH_THIS/status.tmp > /etc/cyberpatriot/IF_YOU_ARE_COMPETING_DO_NOT_MESS_WITH_THIS/status2.txt";
        const char * totalstring = total.c_str();

        system(totalstring);

        ifstream revivelastscore;
        revivelastscore.open("/etc/cyberpatriot/IF_YOU_ARE_COMPETING_DO_NOT_MESS_WITH_THIS/status2.txt");
        getline(revivelastscore, checkpoint);

        if (checkpoint == newerstatus) {
                return 0;
        }

        if (checkpoint != newerstatus) {
                if (checkpoint == newerstatusone) {
                        system("sudo aplay --test-nowait /etc/cyberpatriot/IF_YOU_ARE_COMPETING_DO_NOT_MESS_WITH_THIS/bad.wav");
                }
                if (checkpoint == newerstatuszero) {
                        system("sudo aplay --test-nowait /etc/cyberpatriot/IF_YOU_ARE_COMPETING_DO_NOT_MESS_WITH_THIS/good.wav");
                }
        }

        //cout << "\n\n\n" << checkpoint << "\n\n\n";
};

int savestatus(string name, int status) {
        statusfile << name << "=" << status << endl;
};

int checkupdate (string packagename, string defaultversion, int points, string alias) {
        string total = "sudo apt-cache policy " + packagename + " | grep -i 'installed' > /etc/cyberpatriot/IF_YOU_ARE_COMPETING_DO_NOT_MESS_WITH_THIS/command.txt";
        const char * totalchar = total.c_str();

        system(totalchar);
        ifstream update;
        update.open("/etc/cyberpatriot/IF_YOU_ARE_COMPETING_DO_NOT_MESS_WITH_THIS/command.txt");

        getline(update,command);

        string updateo = "  Installed: " + defaultversion;
        string statusname = packagename + defaultversion;

        if (command != updateo) {
                score += points;
                vulnnum++;
                vulns << "updated " << packagename << " - " << points << " pts" << endl << "<br>\n";

                sounds(alias,1);

                savestatus(alias, 1);
        }
        else {
                sounds(alias,0);

                savestatus(alias, 0);
        }
        update.close();
};

int filecheck(string filelocation, string keep, int points, string alias) {
        string total = "test -f " + filelocation + " && echo \"found\" >> /etc/cyberpatriot/IF_YOU_ARE_COMPETING_DO_NOT_MESS_WITH_THIS/command.txt || echo \"notfound\" >> /etc/cyberpatriot/IF_YOU_ARE_COMPETING_DO_NOT_MESS_WITH_THIS/command.txt";
        const char * totalchar = total.c_str();

        system(totalchar);
        ifstream file;
        file.open("/etc/cyberpatriot/IF_YOU_ARE_COMPETING_DO_NOT_MESS_WITH_THIS/command.txt");

        getline(file,command);

        if (keep == "remove") {
                if (command == "notfound") {
                        score += points;
                        vulnnum++;
                        vulns << "removed file " << filelocation << " - " << points << " pts" << endl << "<br>\n";

                        sounds(alias,1);

                        savestatus(alias, 1);
                }
                else {
                        sounds(alias,0);

                        savestatus(alias, 0);
                }
        }

        if (keep == "add") {
                if (command == "found") {
                        score += points;
                        vulnnum++;
                        vulns << "added file " << filelocation << " - " << points << " pts" << endl << "<br>\n";

                        sounds(alias,1);

                        savestatus(alias, 1);
                }
                else {
                        sounds(alias,0);

                        savestatus(alias, 0);
                }
        }

        if (keep == "keep") {
                if (command == "notfound") {
                        score -= points;
                        vulns << "\n<br><br>	WARNING FILE " << filelocation << " REMOVED" << " - -" << points << " pts\n" << endl << "<br><br><br>\n";
                        system("sudo aplay --test-nowait /etc/cyberpatriot/IF_YOU_ARE_COMPETING_DO_NOT_MESS_WITH_THIS/bad.wav");

                        sounds(alias,0);

                        savestatus(alias, 0);
                }
                else {
                        sounds(alias,1);

                        savestatus(alias, 1);
                }
                //return 0;
        }

        if (keep == "shouldnotadd") {
                if (command == "found") {
                        score -= points;
                        vulns << "\n<br><br>	WARNING FILE " << filelocation << " ADDED" << " - -" << points << " pts\n" << endl << "<br><br><br>\n";
                        system("sudo aplay --test-nowait /etc/cyberpatriot/IF_YOU_ARE_COMPETING_DO_NOT_MESS_WITH_THIS/bad.wav");

                        sounds(alias,0);

                        savestatus(alias, 0);
                }
                else {
                        sounds(alias,1);

                        savestatus(alias, 1);
                }
        }
        file.close();
};

int checkfirewall(string status, int points) {
        string total = "sudo ufw status verbose > /etc/cyberpatriot/IF_YOU_ARE_COMPETING_DO_NOT_MESS_WITH_THIS/command.txt";
        const char * totalchar = total.c_str();

        system(totalchar);
        ifstream file;
        file.open("/etc/cyberpatriot/IF_YOU_ARE_COMPETING_DO_NOT_MESS_WITH_THIS/command.txt");

        getline(file,command);

        if (status == "enable") {
                if (command != "Status: inactive") {
                        score += points;
                        vulnnum++;
                        vulns << "enabled firewall - " << points << " pts" << endl << "<br>\n";

                        sounds("A1638",1);

                        savestatus("A1638", 1);
                }
                else {
                        sounds("A1638",0);

                        savestatus("A1638", 0);
                }
        }

        if (status == "disable") {
                if (command == "Status: inactive") {
                        score += points;
                        vulnnum++;
                        vulns << "disabled firewall - " << points << " pts" << endl << "<br>\n";

                        sounds("A1638",1);

                        savestatus("A1638", 1);
                }
                else {
                        sounds("A1638",0);

                        savestatus("A1638", 0);
                }
        }

        if (status == "keepenabled") {
                if (command == "Status: inactive") {
                        score -= points;
                        vulns << "\n<br><br>	WARNING FIREWALL DISABLED - -" << points << " pts\n" << endl << "<br><br><br>\n";
                        system("sudo aplay --test-nowait /etc/cyberpatriot/IF_YOU_ARE_COMPETING_DO_NOT_MESS_WITH_THIS/bad.wav");

                        sounds("A1638",0);

                        savestatus("A1638", 0);
                }
                else {
                        sounds("A1638",1);

                        savestatus("A1638", 1);
                }
                //return 0;
        }

        if (status == "keepdisabled") {
                if (command != "Status: inactive") {
                        score -= points;
                        vulns << "\n<br><br>	WARNING FIREWALL ENABLED - -" << points << " pts\n" << endl << "<br><br><br>\n";
                        system("sudo aplay --test-nowait /etc/cyberpatriot/IF_YOU_ARE_COMPETING_DO_NOT_MESS_WITH_THIS/bad.wav");

                        sounds("A1638",0);

                        savestatus("A1638", 0);
                }
                else {
                        sounds("A1638",1);

                        savestatus("A1638", 1);
                }
        }
        file.close();
};

int groupcheck(string groupname, string keep, int points, string alias) {
        string total = "egrep -o \"" + groupname + "\" /etc/group > /etc/cyberpatriot/IF_YOU_ARE_COMPETING_DO_NOT_MESS_WITH_THIS/command.txt";
        const char * totalchar = total.c_str();

        system(totalchar);
        ifstream package;
        package.open("/etc/cyberpatriot/IF_YOU_ARE_COMPETING_DO_NOT_MESS_WITH_THIS/command.txt");

        getline(package,command);

        if (keep == "add") {
                if (command == groupname) {
                        score += points;
                        vulnnum++;
                        vulns << "added group " << groupname << " - " << points << " pts" << endl << "<br>\n";

                        sounds(alias,1);

                        savestatus(alias, 1);
                }
                else {
                        sounds(alias,0);

                        savestatus(alias, 0);
                }
                //return 0;
        }

        //else savestatus(packagename, 0);

        if (keep == "remove") {
                if (command != groupname) {
                        score += points;
                        vulnnum++;
                        vulns << "removed group " << groupname << " - " << points << " pts" << endl << "<br>\n";

                        sounds(alias,1);

                        savestatus(alias, 1);
                }
                else {
                        sounds(alias,0);

                        savestatus(alias, 0);
                }
                //return 0;
        }

        if (keep == "donotadd") {
                if (command == groupname) {
                        score -= points;
                        vulns << "\n	WARNING GROUP " << groupname << " ADDED" << " - -" << points << " pts\n" << endl << "<br>\n";
                        system("sudo aplay --test-nowait /etc/cyberpatriot/IF_YOU_ARE_COMPETING_DO_NOT_MESS_WITH_THIS/bad.wav");

                        sounds(alias,0);

                        savestatus(alias, 0);
                }
                else {
                        sounds(alias,1);

                        savestatus(alias, 1);
                }
                //return 0;
        }

        if (keep == "donotremove") {
                if (command != groupname) {
                        score -= points;
                        vulns << "\n	WARNING GROUP " << groupname << " REMOVED" << " - -" << points << " pts\n" << endl << "<br>\n";
                        system("sudo aplay --test-nowait /etc/cyberpatriot/IF_YOU_ARE_COMPETING_DO_NOT_MESS_WITH_THIS/bad.wav");
                        sounds(alias,0);

                        savestatus(alias, 0);
                }
                else {
                        sounds(alias,1);

                        savestatus(alias, 1);
                }
        }
        package.close();
};

int checkpasswordlock(string accountname, string keep, int points, string alias) {
        string total = "sudo passwd -S " + accountname + " > /etc/cyberpatriot/IF_YOU_ARE_COMPETING_DO_NOT_MESS_WITH_THIS/command.txt";
        const char * totalchar = total.c_str();

        string savename = accountname + "lock";

        system(totalchar);

        string total2 = "sudo grep -o \"L\" /etc/cyberpatriot/IF_YOU_ARE_COMPETING_DO_NOT_MESS_WITH_THIS/command.txt > /etc/cyberpatriot/IF_YOU_ARE_COMPETING_DO_NOT_MESS_WITH_THIS/command2.txt";
        const char * totalchar2 = total2.c_str();

        system(totalchar2);

        ifstream package;
        package.open("/etc/cyberpatriot/IF_YOU_ARE_COMPETING_DO_NOT_MESS_WITH_THIS/command2.txt");

        getline(package,command);

        if (keep == "lock") {
                if (command == "L") {
                        score += points;
                        vulnnum++;
                        vulns << "Locked user " << accountname << " - " << points << " pts" << endl << "<br>\n";

                        sounds(alias,1);

                        savestatus(alias, 1);
                }
                else {
                        sounds(alias,0);

                        savestatus(alias, 0);
                }
        }

        if (keep == "unlock") {
                if (command != "L") {
                        score += points;
                        vulnnum++;
                        vulns << "unlocked user " << accountname << " - " << points << " pts" << endl << "<br>\n";


                        sounds(alias,1);

                        savestatus(alias, 1);
                }
                else {
                        sounds(alias,0);

                        savestatus(alias, 0);
                }
        }

        if (keep == "donotlock") {
                if (command == "L") {
                        score -= points;
                        vulns << "\n	WARNING USER " << accountname << " ACCOUNT LOCKED" << " - -" << points << " pts\n" << endl << "<br>\n";
                        system("sudo aplay --test-nowait /etc/cyberpatriot/IF_YOU_ARE_COMPETING_DO_NOT_MESS_WITH_THIS/bad.wav");


                        sounds(alias,0);

                        savestatus(alias,0);
                }
                else {
                        sounds(alias,1);

                        savestatus(alias,1);
                }
        }

        if (keep == "donotunlock") {
                if (command != "L") {
                        score -= points;
                        vulns << "\n	WARNING USER " << accountname << " ACCOUNT UNLOCKED" << " - -" << points << " pts\n" << endl << "<br>\n";
                        system("sudo aplay --test-nowait /etc/cyberpatriot/IF_YOU_ARE_COMPETING_DO_NOT_MESS_WITH_THIS/bad.wav");
                        sounds(alias,0);

                        savestatus(alias,0);
                }
                else {
                        sounds(alias,1);

                        savestatus(alias,1);
                }
        }
        package.close();
};

int checkcrontab(string configsetting, string croncommand, string configname, string configstatus, int points, string alias) {
        string total = 	"sudo grep \"" + croncommand + "\" /etc/cyberpatriot/IF_YOU_ARE_COMPETING_DO_NOT_MESS_WITH_THIS/command.txt > /etc/cyberpatriot/IF_YOU_ARE_COMPETING_DO_NOT_MESS_WITH_THIS/command2.txt";
        const char * totalchar = total.c_str();

        system("sudo crontab -l > /etc/cyberpatriot/IF_YOU_ARE_COMPETING_DO_NOT_MESS_WITH_THIS/command.txt");

        system(totalchar);

        ifstream config;
        config.open("/etc/cyberpatriot/IF_YOU_ARE_COMPETING_DO_NOT_MESS_WITH_THIS/command2.txt");
        getline(config,command);

        //cout << endl << command << endl;

        if (configstatus == "add") {
                if (command == configsetting) {
                        score += points;
                        vulnnum++;
                        vulns << "Set crontab configuration setting: " << configname << " - " << points << " pts" << endl << "<br>\n";
                        sounds(alias,1);

                        savestatus(alias, 1);
                }
                else {
                        sounds(alias,0);

                        savestatus(alias, 0);
                }
        }

        if (configstatus == "remove") {
                if (command != configsetting) {
                        score += points;
                        vulnnum++;
                        vulns << "Removed crontab configuration setting: " << configname << " - " << points << " pts" << endl << "<br>\n";
                        sounds(alias,1);

                        savestatus(alias, 1);
                }
                else {
                        sounds(alias,0);

                        savestatus(alias, 0);
                }
        }

        if (configstatus == "shouldnotremove") {
                if (command != configsetting) {
                        score -= points;
                        vulns << "\n	WARNING CRONTAB CONFIGURATION REMOVED: " << configname << " - -" << points << " pts\n" << endl << "<br>\n";
                        system("sudo aplay --test-nowait /etc/cyberpatriot/IF_YOU_ARE_COMPETING_DO_NOT_MESS_WITH_THIS/bad.wav");
                        sounds(alias,0);

                        savestatus(alias, 0);
                }
                else {
                        sounds(alias,1);

                        savestatus(alias, 1);
                }
        }

        if (configstatus == "shouldnotadd") {
                if (command == configsetting) {
                        score -= points;
                        vulns << "\n	WARNING CRONTAB CONFIGURATION ADDED: " << configname << " - -" << points << " pts\n" << endl << "<br>\n";
                        system("sudo aplay --test-nowait /etc/cyberpatriot/IF_YOU_ARE_COMPETING_DO_NOT_MESS_WITH_THIS/bad.wav");
                        sounds(alias,0);

                        savestatus(alias, 0);
                }
                else {
                        sounds(alias,1);

                        savestatus(alias, 1);
                }
        }

        config.close();
};
